import isMobile from '@/helpers/isMobile';
import getCookie from '@/helpers/getCookie';

export default {
  head() {
    return {
      title: 'TERVOLINA',
      meta: [
        {
          hid: 'og:title',
          property: 'og:title',
          content: 'TERVOLINA',
        },
        {
          hid: 'og:description',
          property: 'og:description',
          content: 'Tervolina.ru - официальный интернет-магазин модной женской и мужской обуви, сумок и аксессуаров Tervolina. Доставка по Москве и России. Тел. 8 800 200 81 17',
        },
        {
          hid: 'og:image',
          property: 'og:image',
          content: '/og.png',
        },
        {
          hid: 'og:url',
          property: 'og:url',
          content: 'https://www.tervolina.ru',
        },
      ],
    };
  },
  computed: {
    errors() {
      return this.$store.state.errors;
    },

    sessionId() {
      return this.$store.state.sessionId;
    },
  },
  watch: {
    // eslint-disable-next-line no-unused-vars
    errors(errors) {
      // if (!errors || !errors.length) { return; }
      //
      // const message = [...errors.map(e => e.message)].join('.\r\n');
      //
      // this.$modal.show('info', {
      //   title: 'Ошибка',
      //   text: message,
      // });
    },
  },
  beforeMount() {
    this.checkedMobile();
    window.addEventListener('resize', this.checkedMobile);
  },
  mounted() {
    if (this.sessionId) {
      document.cookie = `PHPSESSID=${this.sessionId}`;
    }
    if (
      getCookie('confirmedCity')
      && window.localStorage
      && localStorage.getItem('confirmedCity')
    ) {
      const city = JSON.parse(localStorage.getItem('confirmedCity'));
      this.$store.commit('user/FETCHED_CITY', {
        data: city.data,
        value: city.value,
      });
      this.$store.commit('user/FETCHED_PHONE', city.phone);
    }

    this.$store.commit('SET_COOKIE_PERMISSON', getCookie('cookiePermission'));
  },
  destroyed() {
    window.removeEventListener('resize', this.checkedMobile);
  },
  methods: {
    checkedMobile() {
      const result = window.innerWidth < 1180 || isMobile();
      this.$store.commit('layout/SET_MOBILE', result);
    },
  },
};
