import { cacheAdapterEnhancer } from 'axios-extensions';
import LRUCache from 'lru-cache';

const ONE_HOUR = 1000 * 60 * 60;
const defaultCache = new LRUCache({ maxAge: ONE_HOUR });

const token = '86a8056700b571f499c5593b0f905820';
const login = 'studio';
const pass = 'ratio';

export default function ({ app, store, $axios }) {
  const { defaults } = app.$axios;

  // https://github.com/kuitos/axios-extensions
  defaults.adapter = cacheAdapterEnhancer(defaults.adapter, {
    enabledByDefault: false,
    cacheFlag: 'useCache',
    defaultCache,
  });

  $axios.onRequest((config) => {
    const newConfig = config;

    newConfig.headers = {
      ...config.headers,
      'Content-Type': 'application/json',
    };

    if (config.url.match(/^frontend\//) || config.url.match(/^\/frontend\//)) {
      newConfig.auth = {
        username: login,
        password: pass,
      };

      newConfig.params = {
        ...config.params,
        token,
      };
    }

    return newConfig;
  });

  $axios.onResponse((reponse) => {
    if (reponse.data.DATA && reponse.data.DATA.sessionId) {
      store.commit('SET_SESSION_ID', reponse.data.DATA.sessionId);
    }
  });

  $axios.onError((e) => {
    let text = 'Неизвестная ошибка';

    if (e.response && e.response.data) {
      text = e.response.data.MESSAGE || e.response.data.message;
    } else if (e.message) {
      text = e.message;
    }

    store.commit('ADD_ERROR', text);

    return Promise.reject(new Error(text));
  });
}
