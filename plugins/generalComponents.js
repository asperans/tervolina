/* eslint-disable import/no-extraneous-dependencies */
import Vue from 'vue';

// Компоненты общего назначения
import AppIcon from '@/components/general/AppIcon';
import AppCounter from '@/components/general/AppCounter';
import AppDropout from '@/components/general/AppDropout';
import AppButton from '@/components/general/AppButton';
import AppInput from '@/components/general/AppInput';
import AppCheckbox from '@/components/general/AppCheckbox';
import AppTextarea from '@/components/general/AppTextarea';
import AppBreadcrumbs from '@/components/general/AppBreadcrumbs';
import AppFilter from '@/components/general/AppFilter';
import AppColorIndicator from '@/components/general/AppColorIndicator';
import AppStarRating from '@/components/general/AppStarRating';
import AppItemBadges from '@/components/general/AppItemBadges';
import AppTabs from '@/components/general/AppTabs';
import AppPagenSimple from '@/components/general/AppPagenSimple';
import AppStepCircle from '@/components/general/AppStepCircle';
import AppLink from '@/components/general/AppLink';
import AppDatePicker from '@/components/general/AppDatePicker';
import AppRadio from '@/components/general/AppRadio';
import AppMetroIndicator from '@/components/general/AppMetroIndicator';
import AppSocials from '@/components/general/AppSocials';
import AppProductList from '@/components/general/AppProductList';
import AppProductItem from '@/components/general/AppProductItem';
import AppSuggestions from '@/components/general/AppSuggestions';
import AppMobileFilter from '@/components/general/AppMobileFilter';
import AppMagnifier from '@/components/general/AppMagnifier';

// Плагины
import vClickOutside from 'v-click-outside';
import VModal from 'vue-js-modal/dist/ssr.index';
import Multiselect from 'vue-multiselect';
import Vuebar from 'vuebar';
import SlideUpDown from 'vue-slide-up-down';
import Vuelidate from 'vuelidate';
import VTooltip from 'v-tooltip';
import VueTheMask from 'vue-the-mask';

// Компоненты общего назначения
Vue.component('app-icon', AppIcon);
Vue.component('app-counter', AppCounter);
Vue.component('app-dropout', AppDropout);
Vue.component('app-button', AppButton);
Vue.component('app-input', AppInput);
Vue.component('app-date-picker', AppDatePicker);
Vue.component('app-checkbox', AppCheckbox);
Vue.component('app-textarea', AppTextarea);
Vue.component('app-breadcrumbs', AppBreadcrumbs);
Vue.component('app-filter', AppFilter);
Vue.component('app-mobile-filter', AppMobileFilter);
Vue.component('app-color-indicator', AppColorIndicator);
Vue.component('app-star-rating', AppStarRating);
Vue.component('app-item-badges', AppItemBadges);
Vue.component('app-tabs', AppTabs);
Vue.component('app-pagen-simple', AppPagenSimple);
Vue.component('app-step-circle', AppStepCircle);
Vue.component('app-link', AppLink);
Vue.component('app-radio', AppRadio);
Vue.component('app-metro-indicator', AppMetroIndicator);
Vue.component('app-socials', AppSocials);
Vue.component('app-product-list', AppProductList);
Vue.component('app-product-item', AppProductItem);
Vue.component('app-suggestions', AppSuggestions);
Vue.component('app-magnifier', AppMagnifier);

// Плагины
Vue.component('multiselect', Multiselect);
Vue.component('slide-up-down', SlideUpDown);

Vue.use(vClickOutside);
Vue.use(VModal, { dynamic: true });
Vue.use(Vuebar);
Vue.use(Vuelidate);
Vue.use(VTooltip);
Vue.use(VueTheMask);
