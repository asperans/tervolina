import Swiper from 'swiper';

export default class {
  constructor($c, params = {}) {
    this.el = $c[0] ? $c[0] : $c;

    const type = this.el.getAttribute('data-type');

    this.swiper = new Swiper(this.el, this.getParameters(type, params));
  }

  getParameters(type, addParams) {
    const parameters = {
      pagination: {
        el: this.el.querySelector('.js-slider-pagination'),
        bulletClass: 'bullet',
        bulletActiveClass: '_active',
      },
      noSwipingClass: 'js-slider-no-swiping',
    };

    switch (type) {
      case 'index-promo':
        Object.assign(parameters, {
          slidesPerView: 1,
          navigation: {
            prevEl: this.el.parentNode.querySelector('.prev'),
            nextEl: this.el.parentNode.querySelector('.next'),
          },
        });
        break;

      case 'index-promo-minor':
        Object.assign(parameters, {
          slidesPerView: 1,
          spaceBetween: 16,
          pagination: {
            el: this.el.parentNode.querySelector('.swiper-pagination'),
            type: 'progressbar',
          },
        });
        break;

      case 'last-articles-index':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 16,
          pagination: {
            el: this.el.parentNode.querySelector('.swiper-pagination'),
            type: 'progressbar',
          },
        });
        break;

      case 'popular-articles-index':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 16,
          loop: true,
          pagination: {
            el: this.el.parentNode.querySelector('.swiper-pagination'),
            clickable: true,
          },
        });
        break;

      case 'new-products':
        Object.assign(parameters, {
          slidesPerView: 4,
          navigation: {
            prevEl: this.el.parentNode.parentNode.querySelector('.prev'),
            nextEl: this.el.parentNode.parentNode.querySelector('.next'),
          },
          breakpoints: {
            1179: {
              slidesPerView: 3,
              spaceBetween: 30,
            },
            767: {
              loop: false,
              slidesPerView: 2,
              slidesPerColumn: 2,
              spaceBetween: 16,
            },
          },
        });
        break;

      case 'instagram':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 42,
          loop: true,
          navigation: {
            prevEl: this.el.parentNode.parentNode.querySelector('.prev'),
            nextEl: this.el.parentNode.parentNode.querySelector('.next'),
          },
          breakpoints: {
            1179: {
              spaceBetween: 20,
            },
            767: {
              loop: false,
              slidesPerView: 2,
              slidesPerColumn: 2,
              spaceBetween: 16,
            },
          },
        });
        break;

      case 'detail-main-top-slider':
        Object.assign(parameters, {
          slidesPerView: 1,
          navigation: {
            prevEl: this.el.parentNode.querySelector('.prev'),
            nextEl: this.el.parentNode.querySelector('.next'),
          },
        });
        break;

      case 'detail-main-bottom-slider':
        Object.assign(parameters, {
          slidesPerView: 'auto',
          spaceBetween: 8,
          watchSlidesVisibility: true,
          watchSlidesProgress: true,
        });
        break;

      case 'blog-detail-slider':
        Object.assign(parameters, {
          slidesPerView: 1,
          navigation: {
            prevEl: this.el.parentNode.parentNode.querySelector('.prev'),
            nextEl: this.el.parentNode.parentNode.querySelector('.next'),
          },
        });
        break;

      default:
    }

    if (addParams) {
      Object.assign(parameters, addParams);
    }

    return parameters;
  }
}
