export default function ({ store, redirect }) {
  if (!store.state.basket.products.total) {
    redirect('/');
  }
}
