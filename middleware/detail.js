export default function ({ store, route }) {
  const departments = Array.from(store.state.layout.departments.data);
  const currentDepartment = departments
    .find(({ url }) => {
      const map = {
        '/': '/catalog/zhenshchinam/',
        '/man/': '/catalog/man/',
        '/children/': '/catalog/children/',
      };
      return route.path.indexOf(map[url]) !== -1;
    }) || departments[0];
  const currentCategory = currentDepartment.categories
    .find(({ url }) => route.path.indexOf(url) !== -1);
  const currentSubCategory = currentCategory.subCategories
    ? currentCategory.subCategories
      .find(({ url }) => route.path.indexOf(url) !== -1)
    : {};

  store.commit('layout/SET_CURRENT_DEPARTMENT', currentDepartment);
  store.commit('catalogPage/SET_SELECTED_CATEGORY', currentCategory);
  store.commit('catalogPage/SET_SELECTED_SUBCATEGORY', currentSubCategory);
}
