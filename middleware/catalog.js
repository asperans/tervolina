export default function ({ store, route, redirect }) {
  const departments = Array.from(store.state.layout.departments.data);
  const currentDepartment = departments
    .find(({ url }) => {
      const map = {
        '/': '/catalog/zhenshchinam/',
        '/man/': '/catalog/man/',
        '/children/': '/catalog/children/',
      };
      return route.path.indexOf(map[url]) !== -1;
    }) || departments[0];

  if (!currentDepartment || !currentDepartment.categories) {
    redirect('/');
  }

  const currentCategory = currentDepartment.categories
    .find(({ url }) => route.path.indexOf(url) !== -1)
    || currentDepartment.categories[0];
  const currentSubCategory = currentCategory.subCategories
    .find(({ url }) => route.path.indexOf(url) !== -1)
      || {};

  if (!route.params.category && currentCategory.url) {
    if (!route.params.subcategory && currentSubCategory.url) {
      redirect(currentSubCategory.url);
    }
    redirect(currentCategory.url);
  }


  const subCategories = currentCategory.subCategories.reduce((acc, sub) => [
    ...acc,
    sub.id,
  ], []);

  const paramsSubCategories = {
    'section-id': subCategories,
  };

  const paramsSection = currentSubCategory.id
    ? { 'section-id': currentSubCategory.id }
    : { ...paramsSubCategories };


  return new Promise((resolve) => {
    store.commit('layout/SET_CURRENT_DEPARTMENT', currentDepartment);
    resolve();
  })
    .then(() => store
      .dispatch(
        'catalogPage/SELECT_CATEGORY',
        route.params.category ? currentCategory : {},
      ))
    .then(() => store
      .dispatch(
        'catalogPage/SELECT_SUBCATEGORY',
        route.params.subcategory ? currentSubCategory : {},
      ))
    .then(() => {
      const { detailUrl } = store.state.catalogPage.backToCatalog;
      if (detailUrl.indexOf(route.params.subcategory) !== -1) {
        return new Promise((resolve) => {
          resolve();
        });
      }

      // check sort by query and set
      const { sortBy: by = '', sortOrder: order = '' } = route.query;
      const checkSort = by || order;
      store.commit('catalogPage/SET_SORT', checkSort
        ? { by, order }
        : {});

      // reset page
      store.commit('catalogPage/SET_PAGE', 1);

      // check filters in query
      const keysQuery = Object.keys(route.query);
      const checkFilters = keysQuery
        .find(key => route.query[key].length || key === 'minPrice' || key === 'maxPrice');

      // TODO: Сделано временно для получения NEW и SALES
      const diffCategory = {
        ...(currentCategory.id === 'new' ? { isNew: true } : {}),
        ...(currentCategory.id === 'sale' ? { isSale: true } : {}),
      };

      // build params
      const params = {
        ...diffCategory,
        ...paramsSection,
        page: store.state.catalogPage.page,
        limit: 40,
      };

      if (!checkFilters) {
        return store.dispatch('catalogPage/FETCH_PRODUCTS', {
          ...params,
        });
      }
      return store.dispatch('catalogPage/FETCH_FILTERED_PRODUCTS', {
        ...params,
        query: {
          ...route.query,
        },
      });
    })
    .then(() => store.dispatch('catalogPage/FETCH_FILTERS', {
      ...paramsSection,
    }))
    .then(() => store.dispatch('catalogPage/FETCH_BANNER', {
      departmentId: currentDepartment.id,
      categoryId: currentCategory.id,
      ...(currentSubCategory.id ? { subCategoryId: currentSubCategory.id } : {}),
    }))
    .catch(() => {});
}
