const express = require('express');

const router = express.Router();

// json
const sizes = require('../assets/json/reservation/sizes.json');
const shops = require('../assets/json/reservation/shops.json');

router.get('/sizes', (req, res) => {
  res.json(sizes);
});

router.get('/shops', (req, res) => {
  res.json(shops);
});

module.exports = router;
