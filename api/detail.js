const express = require('express');

const router = express.Router();

// json
const index = require('../assets/json/detail/index.json');
const benefits = require('../assets/json/detail/benefits.json');
const buyTogetherCheaper = require('../assets/json/detail/buyTogetherCheaper.json');
const latest = require('../assets/json/detail/latest.json');
const similar = require('../assets/json/detail/similar.json');
const suggestion = require('../assets/json/detail/suggestion.json');
const tabs = require('../assets/json/detail/tabs.json');

router.get('/', (req, res) => {
  res.json(index);
});

router.get('/benefits', (req, res) => {
  res.json(benefits);
});

router.get('/buyTogetherCheaper', (req, res) => {
  res.json(buyTogetherCheaper);
});

router.get('/latest', (req, res) => {
  res.json(latest);
});

router.get('/similar', (req, res) => {
  res.json(similar);
});

router.get('/suggestion', (req, res) => {
  res.json(suggestion);
});

router.get('/tabs', (req, res) => {
  res.json(tabs);
});

module.exports = router;
