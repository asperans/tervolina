const express = require('express');

const router = express.Router();

// json
const categories = require('../assets/json/catalog/categories.json');
const products = require('../assets/json/catalog/products.json');

router.get('/categories', (req, res) => {
  res.json(categories);
});

router.get('/products', (req, res) => {
  res.json(products);
});

module.exports = router;
