const express = require('express');

const app = express();

// routing
const catalog = require('./catalog');
const detail = require('./detail');
const reservation = require('./reservation');

app.get('/', (req, res) => {
  res.send('API root');
});

app.use('/catalog', catalog);
app.use('/detail', detail);
app.use('/reservation', reservation);

// export the server middleware
module.exports = {
  path: '/api',
  handler: app,
};
