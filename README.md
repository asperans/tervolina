# tervolina

> Фронтенд проекта Tervolina

## Build Setup

``` bash
# Установить зависимости
$ yarn

# Запуск локального сервера по адресу localhost:5000
$ yarn dev

# Собрать и запустить продакшен версию
$ yarn build
$ yarn start

# Собрать и запустить продакшен версию через pm2 на деве
# Когда админы глобально установят, pm2 можно будет запускать без npx
$ yarn build
$ npx pm2 start

# Собрать и запустить продакшен версию через pm2 на винде
$ yarn build
$ npx pm2 start startscript.js

# Показать информацию о процессе через ID
$ npx pm2 show 0

# Перезапустить процесс через ID
$ npx pm2 reload 0

# Удалить процесс через ID
$ npx pm2 delete 0

# Быстрый запуск на сервере
$ npx pm2 del 0 && yarn && yarn build && npx pm2 start

```

## Configuration

В файлах по адресу **/web/config/** лежат json-файлы для перекрытия некоторых полей nuxt.config.js
**config.json** - основной файл
**config-default.json** - используется, если первый отсутствует
При добавлениее новых полей надо добавить соответсвующую ссылку в сам nuxt.config.js.
Например:
```
...
proxy: external.proxy,
...
```

Более подробно про PM2: [PM2](http://pm2.keymetrics.io/)
Более подробно про Nuxt: [Nuxt.js docs](https://nuxtjs.org).
