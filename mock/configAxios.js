const mockConfigAxios = {
  useCache: true,
  baseURL: process.env.NODE_ENV === 'development'
    ? 'http://localhost:5000/api'
    : 'http://tervolina.ratio.bz/api',
};

export default mockConfigAxios;
