/* eslint-disable no-shadow */
// Модуль для работы с оформлением заказа
export const state = () => ({
  steps: {
    delivery: {
      title: 'Доставка покупок',
      component: 'CheckoutDelivery',
      status: 'active',
      price: 0,
      type: '',
    },
    payment: {
      title: 'Оплата',
      component: 'CheckoutPayment',
      status: 'disabled',
      type: '',
    },
    contacts: {
      title: 'Контакты',
      component: 'CheckoutContacts',
      status: 'disabled',
    },
  },
});

export const mutations = {
  SET_STEP_TITLE(state, payload) {
    state.steps[payload.step].title = payload.value;
  },

  SET_STEP_STATUS(state, payload) {
    state.steps[payload.step].status = payload.value;
  },

  SET_STEP_PRICE(state, payload) {
    state.steps[payload.step].price = payload.value;
  },

  SET_STEP_TYPE(state, payload) {
    state.steps[payload.step].type = payload.value;
  },

  RESET_STEPS(state) {
    state.steps = {
      delivery: {
        title: 'Доставка покупок',
        component: 'CheckoutDelivery',
        status: 'active',
        price: 0,
        type: '',
      },
      payment: {
        title: 'Оплата',
        component: 'CheckoutPayment',
        status: 'disabled',
        type: '',
      },
      contacts: {
        title: 'Контакты',
        component: 'CheckoutContacts',
        status: 'disabled',
      },
    };
  },
};

export const actions = {
  COMPLETE_STEP({ commit }, params) {
    switch (params.step) {
      case 'delivery':
        commit('SET_STEP_PRICE', { step: params.step, value: params.price });
        commit('SET_STEP_TYPE', { step: params.step, value: params.type });
        commit('SET_STEP_STATUS', { step: 'payment', value: 'active' });
        break;
      case 'payment':
        commit('SET_STEP_TYPE', { step: params.step, value: params.type });
        commit('SET_STEP_STATUS', { step: 'contacts', value: 'active' });
        break;
      default:
    }

    commit('SET_STEP_STATUS', { step: params.step, value: 'completed' });
    commit('SET_STEP_TITLE', { step: params.step, value: params.title });
  },

  EDIT_STEP({ commit }, params) {
    switch (params.step) {
      case 'delivery':
        commit('SET_STEP_STATUS', { step: 'payment', value: 'disabled' });
        commit('SET_STEP_STATUS', { step: 'contacts', value: 'disabled' });
        break;
      case 'payment':
        commit('SET_STEP_STATUS', { step: 'contacts', value: 'disabled' });
        break;
      default:
    }

    commit('SET_STEP_STATUS', { step: params.step, value: 'active' });
  },
};
