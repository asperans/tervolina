/* eslint-disable no-shadow */
// Модуль для работы с пользовательскими данными
export const state = () => ({
  personal: {
    isFetching: false,
    data: {
      addresses: [{}],
    },
  },
  city: {
    isFetching: false,
    data: {},
    name: '',
    value: '',
    fias: '',
    phone: {
      raw: '',
      formatted: '',
    },
  },
  cityList: {
    isFetching: false,
    data: [],
  },
  isAuthorized: false,
  registrationStep: 'personal',
});

export const mutations = {
  SET_PERSONAL(state, payload) {
    state.personal[payload.key] = payload.value;
  },

  SET_REGISTRATION_STEP(state, payload) {
    state.registrationStep = payload;
  },

  FETCHING_PERSONAL(state) {
    state.personal.isFetching = true;
  },

  FETCHED_PERSONAL(state, payload) {
    state.personal.isFetching = false;
    state.isAuthorized = payload.isAuthorized;
    state.personal.data = {
      ...state.personal.data,
      ...(payload.user ? payload.user : {}),
      ...(payload.balance ? { balance: payload.balance } : {}),
    };
  },

  SET_PHOTO(state, payload) {
    state.personal.photo = payload;
  },

  ADD_BLANK_ADDRESS(state) {
    state.personal.data.addresses = [...state.personal.data.addresses, {}];
  },

  UPDATE_ADDRESS(state, payload) {
    state.personal.data.addresses = state.personal.data.addresses.map((item, index) => {
      const newItem = item;

      if (payload.orderIndex === index) {
        newItem.city = payload.city;
        newItem.index = payload.index;
        newItem.address = payload.address;
      }

      return newItem;
    });
  },

  DELETE_ADDRESS(state, dIndex) {
    state.personal.data.addresses = state
      .personal.data.addresses.filter((item, index) => index !== dIndex);
  },

  FETCHING_CITY(state) {
    state.city.isFetching = true;
  },

  FETCHED_CITY(state, payload) {
    state.city.isFetching = false;
    state.city.data = payload.data;
    state.city.name = payload.data.city ? payload.data.city : payload.data.settlementWithType;
    state.city.value = payload.value;
    state.city.fias = payload.data.city_fias_id ? payload.data.city_fias_id
      : payload.data.cityFiasId;
  },

  FETCHED_PHONE(state, payload) {
    state.city.phone = payload;
  },

  FETCHING_CITY_LIST(state) {
    state.cityList.isFetching = true;
  },

  FETCHED_CITY_LIST(state, payload) {
    state.cityList.isFetching = false;
    state.cityList.data = payload;
  },
};

export const actions = {
  FETCH_PERSONAL({ commit }) {
    commit('FETCHING_PERSONAL');
    return this.$axios.$get('frontend/v1/user/authorization/')
      .then(({ DATA }) => {
        commit('FETCHED_PERSONAL', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_CITY({ commit }) {
    commit('FETCHING_CITY');
    return this.$axios.$get('frontend/v1/user/location/')
      .then(({ DATA }) => {
        commit('FETCHED_CITY', {
          data: DATA.location.data,
          value: DATA.location.value,
        });
        commit('FETCHED_PHONE', DATA.phone);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_CITY_LIST({ commit }) {
    commit('FETCHING_CITY_LIST');
    return this.$axios.$get('frontend/v1/cities/index/')
      .then(({ DATA }) => {
        commit('FETCHED_CITY_LIST', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  LOGIN({ commit }, data) {
    commit('FETCHING_PERSONAL');
    return this.$axios.$post('frontend/v1/user/authorization/', data)
      .then((response) => {
        commit('FETCHED_PERSONAL', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  LOGOUT({ commit }) {
    commit('FETCHING_PERSONAL');
    return this.$axios.$delete('frontend/v1/user/authorization/')
      .then((response) => {
        commit('FETCHED_PERSONAL', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  REGISTRATION_PERSONAL({ commit }, data) {
    return this.$axios.$post('frontend/v1/user/registration/start/', data)
      .then(() => {
        commit('SET_REGISTRATION_STEP', 'code');
      })
      .catch(error => Promise.reject(error));
  },

  REGISTRATION_CODE({ commit }, data) {
    commit('FETCHING_PERSONAL');
    return this.$axios.$post('frontend/v1/user/registration/confirm-phone/', data)
      .then((response) => {
        commit('FETCHED_PERSONAL', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  UPLOAD_PHOTO(context, data) {
    return this.$axios.$post('frontend/v1/user/upload-photo/', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .catch(error => Promise.reject(error));
  },

  UPDATE_USER(context, data) {
    return this.$axios.$post('frontend/v1/user/update/', data)
      .catch(error => Promise.reject(error));
  },

  UPDATE_ADDRESSES({ state }) {
    return this.$axios.$post('frontend/v1/user/addresses/', state.personal.data.addresses)
      .catch(error => Promise.reject(error));
  },

  RECOVER_PASSWORD(context, data) {
    return this.$axios.$post('frontend/v1/user/password/reset/', data)
      .then(response => response)
      .catch(error => Promise.reject(error));
  },

  CONFIRM_PASSWORD({ commit }, data) {
    return this.$axios.$post('frontend/v1/user/password/confirm/', data)
      .then((response) => {
        commit('FETCHED_PERSONAL', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },


  CARD_EMIT(all, data) {
    return this.$axios.$post('frontend/v1/user/card/emit/', data)
      .then(({ MESSAGE }) => new Promise(resolve => resolve(MESSAGE)))
      .catch(error => Promise.reject(error));
  },


  CONFIRM_PHONE({ commit }, data) {
    return this.$axios.$post('frontend/v1/user/card/confirm-phone/', data)
      .then(({ DATA }) => {
        commit('FETCHED_PERSONAL', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
