/* eslint-disable no-shadow */
// Модуль для работы с бонусами
export const state = () => ({
  shops: {
    data: [],
    isFetching: false,
  },
  isLoading: false,
});

export const mutations = {
  LOADING(state) {
    state.isLoading = true;
  },

  LOADED(state) {
    state.isLoading = false;
  },

  FETCHING_SHOPS(state) {
    state.shops.isFetching = true;
  },

  FETCHED_SHOPS(state, data) {
    state.shops.isFetching = false;
    state.shops.data = data;
  },
};


export const actions = {

  FETCH_SHOPS({ commit }, city) {
    commit('FETCHING_SHOPS');
    return this.$axios.$post('frontend/v1/stores/index/', {
      city,
    })
      .then(({ DATA }) => {
        commit('FETCHED_SHOPS', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
