/* eslint-disable no-shadow */
// Модуль для работы с оформлением заказа
export const state = () => ({
  doneProducts: {
    data: {},
    total: 0,
  },
  products: {
    isFetching: false,
    data: {},
    removed: [],
    total: 0,
    oldTotal: 0,
  },
  personal: {
    isFetching: false,
    data: {},
  },
  bonusPaymentLimits: {},
  bonus: 0,
  bonusPay: 0,
});

export const mutations = {
  SET_DONE_PRODUCTS(state, { basketItems, total }) {
    state.doneProducts = {
      ...state.doneProducts,
      data: { ...basketItems },
      total,
    };
  },

  FETCHING_PRODUCTS(state) {
    state.products.isFetching = true;
  },

  FETCHED_UPDATE_PRODUCT(state, {
    basketItems, total, oldTotal, bonusPay, bonus,
  }) {
    state.products = {
      ...state.products,
      isFetching: false,
      data: { ...basketItems },
      total,
      oldTotal,
    };
    state.bonusPay = bonusPay || state.bonusPay;
    state.bonus = bonus || state.bonus;
  },

  REMOVE_PRODUCT(state, basketItem) {
    const item = {
      ...basketItem,
      count: 0,
    };
    state.products = {
      ...state.products,
      removed: [
        ...state.products.removed,
        item,
      ],
    };
  },

  RETURN_REMOVED_PRODUCT(state, basketItemId) {
    const removeProducts = state.products.removed
      .filter(({ id }) => id !== basketItemId);
    state.products = {
      ...state.products,
      removed: [
        ...removeProducts,
      ],
    };
  },

  REMOVE_REMOVED(state, basketItemId) {
    const removeProducts = state.products.removed
      .filter(({ id }) => id !== basketItemId);
    state.products = {
      ...state.products,
      removed: [
        ...removeProducts,
      ],
    };
  },

  CLEAR_REMOVED(state) {
    state.products = {
      ...state.products,
      removed: [],
    };
  },

  FETCHING_PERSONAL(state) {
    state.personal.isFetching = true;
  },

  FETCHED_PERSONAL(state, { balance }) {
    state.personal = {
      ...state.personal,
      isFetching: false,
      data: {
        ...state.personal.data,
        balance,
      },
    };
  },

  SET_BONUS_PAY_LIMITS(state, data) {
    state.bonusPaymentLimits = data;
  },
};

export const actions = {
  FETCH_ADD_PRODUCT({ commit }, { id = 0, count = 1 }) {
    commit('FETCHING_PRODUCTS');
    const data = {
      id,
      count,
    };
    return this.$axios.$post('frontend/v1/cart/add/', data)
      .then(({ DATA }) => {
        commit('FETCHED_UPDATE_PRODUCT', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_UPDATE_COUNT_PRODUCT({ commit, state }, { id: productId, count = 1 }) {
    const stateProducts = state.products.data;
    const resultId = +Object.keys(stateProducts)
      .find(key => stateProducts[key].id === productId);

    const data = {
      id: resultId,
      count,
    };

    commit('FETCHING_PRODUCTS');
    return this.$axios.$post('frontend/v1/cart/update/', data)
      .then(({ DATA }) => {
        commit('FETCHED_UPDATE_PRODUCT', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PRODUCTS({ commit }, data = {}) {
    commit('FETCHING_PRODUCTS');
    return this.$axios.$post('frontend/v1/cart/index/', data)
      .then(({ DATA }) => {
        commit('FETCHED_UPDATE_PRODUCT', DATA);
        if (DATA.bonusPaymentLimits) {
          commit('SET_BONUS_PAY_LIMITS', DATA.bonusPaymentLimits);
        }

        if (!DATA.promocode || !DATA.promocode.label) {
          commit('promocode/CLEAR_PROMOCODE', null, { root: true });
          return;
        }

        commit('promocode/FETCHED_PROMOCODE', DATA.promocode, { root: true });
        commit('promocode/SET_SUBMITTED', DATA.promocode, { root: true });
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_REMOVE_PRODUCT({ commit, state }, { ids }) {
    commit('FETCHING_PRODUCTS');

    const basketProducts = state.products.data;
    const keys = Object.keys(basketProducts);
    const result = keys.filter((key) => {
      if (Array.isArray(ids)) {
        return ids.find(id => basketProducts[key].id === id);
      }
      return basketProducts[key].id === ids;
    });

    const data = {
      ids: result,
    };

    return this.$axios.$post('frontend/v1/cart/delete/', data)
      .then(({ DATA }) => {
        commit('REMOVE_PRODUCT', basketProducts[result]);
        commit('FETCHED_UPDATE_PRODUCT', DATA);

        return DATA;
      })
      .catch(error => Promise.reject(error));
  },


  FETCH_RETURN_REMOVED_PRODUCT({ commit }, { id, count = 1 }) {
    commit('FETCHING_PRODUCTS');
    const data = {
      id,
      count,
    };
    return this.$axios.$post('frontend/v1/cart/add/', data)
      .then(({ DATA }) => {
        commit('RETURN_REMOVED_PRODUCT', id);
        commit('FETCHED_UPDATE_PRODUCT', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PERSONAL({ commit }) {
    commit('FETCHING_PERSONAL');
    return this.$axios.$get('frontend/v1/user/personal/data/')
      .then(({ DATA }) => {
        commit('FETCHED_PERSONAL', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};

export const getters = {
  products(state) {
    const removedProducts = state.products.removed;
    const stateProducts = state.products.data;
    const products = Object.keys(stateProducts)
      .map(key => stateProducts[key]);

    return [
      ...removedProducts,
      ...products,
    ];
  },

  productsAmount(state, getters) {
    return getters.products
      .filter(item => item.count)
      .reduce((acc, item) => acc + (1 * item.count), 0);
  },

  doneProducts(state) {
    const stateDoneProducts = state.doneProducts.data;
    const doneProducts = Object.keys(stateDoneProducts)
      .map(key => stateDoneProducts[key]);

    return [
      ...doneProducts,
    ];
  },

  doneProductsAmount(state, getters) {
    return getters.doneProducts
      .reduce((acc, item) => acc + (1 * item.count), 0);
  },
};
