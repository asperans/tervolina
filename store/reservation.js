/* eslint-disable no-shadow */

export const state = () => ({
  step: 'ReservationMap',
  selectedSize: {
    id: 0,
    value: '',
  },
  selectedShop: {
    id: 0,
    value: '',
  },
  selectOrder: {
    isFetching: false,
    data: {},
  },
  orders: {
    isFetching: false,
    data: [],
  },
  totalCount: 0,
});

export const mutations = {
  SET_STEP(state, payload) {
    state.step = payload;
  },

  SET_SELECTED_SIZE(state, { id = '', value = '' }) {
    state.selectedSize = {
      id,
      value,
    };
  },

  SET_SELECTED_SHOP(state, { id, value }) {
    state.selectedShop = {
      id,
      value,
    };
  },

  SET_PERSONAL(state, payload) {
    state.personal[payload.key] = payload.value;
  },

  FETCHING_ADD_ORDER(state) {
    state.selectOrder.isFetching = true;
  },

  FETCHED_ADD_ORDER(state, { id, date }) {
    state.selectOrder.isFetching = false;
    state.selectOrder.data = {
      id,
      date,
    };
  },

  ADD_TOTAL_COUNT(state) {
    state.countTotal += 1;
  },
};

export const actions = {
  FETCH_ADD_ORDER({ commit }, data) {
    commit('FETCHING_ADD_ORDER');
    return this.$axios.$post('frontend/v1/catalog/product/booking/', data)
      .then(({ DATA }) => {
        if (!DATA.success) { return; }
        commit('FETCHED_ADD_ORDER', {
          id: DATA.orderId,
          date: DATA.date,
        });
      })
      .catch((error) => {
        commit('FETCHED_ADD_ORDER', {});
        return Promise.reject(error);
      });
  },
};
