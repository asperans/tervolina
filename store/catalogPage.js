// eslint-disable-next-line import/no-extraneous-dependencies
import { CancelToken } from 'axios';

const initStateFilters = {
  materials: [],
  colors: [],
  sizes: [],
  price: {
    start: 0,
    end: 0,
  },
  seasons: [],
};

const sortAdapter = (state) => {
  const { by: sortBy, order: sortOrder } = state.sort;
  return {
    sortBy,
    sortOrder,
  };
};

/* eslint-disable no-shadow */
export const state = () => ({
  categories: {
    selectedCategory: {},
    selectedSubCategory: {},
  },
  products: {
    isFetching: false,
    cancelToken: () => ({}),
    currentCardType: 'footwear',
    data: [],
    length: 0,
  },
  sort: {
    by: 'popular',
    order: 'desc',
  },
  filters: {
    selected: {
      ...initStateFilters,
    },
    data: {
      ...initStateFilters,
    },
  },
  backToCatalog: {
    detailUrl: '',
    scrollPoint: 0,
  },
  page: 1,
  banners: {
    isFetching: false,
    data: [],
  },
});

export const mutations = {

  SET_SELECTED_CATEGORY(state, id) {
    state.categories.selectedCategory = id;
  },

  SET_SELECTED_SUBCATEGORY(state, subCategory) {
    state.categories.selectedSubCategory = subCategory;
  },

  SET_CARD_TYPE(state, data) {
    state.products.currentCardType = data;
  },

  SET_SORT(state, { by = 'popular', order = 'desc' }) {
    state.sort = {
      ...state.sort,
      by,
      order,
    };
  },

  FETCHING_PRODUCTS(state, cancelToken = null) {
    state.products.isFetching = true;
    state.products.cancelToken = cancelToken;
  },

  FETCHED_PRODUCTS(state, { products, length, page }) {
    state.products = {
      ...state.products,
      isFetching: false,
      page,
      length,
    };

    state.products.data = page > 1
      ? [...state.products.data, ...products]
      : products;
  },

  FETCHED_FILTERS(state, data) {
    state.filters.data = data;
  },

  SET_FILTER(state, { id, type, value }) {
    const field = state.filters.selected[type];
    state.filters.selected[type] = value
      ? [...field, id]
      : field.filter(item => item !== id);
  },

  SET_FILTERS(state, selectedFilters) {
    state.filters.selected = { ...selectedFilters };
  },

  SET_FILTER_PRICE(state, { start, end }) {
    state.filters.selected.price = {
      start,
      end,
    };
  },

  SET_SELECTED_FILTERS(state, filters) {
    Object.keys(state.filters.selected)
      .forEach((key) => {
        if (filters[key]) {
          const filterBody = Array.isArray(filters[key])
            ? filters[key]
            : [filters[key]];

          state.filters.selected = {
            ...state.filters.selected,
            [key]: filterBody,
          };
        }
      });
    if (filters.minPrice || filters.maxPrice) {
      state.filters.selected.price = {
        start: filters.minPrice,
        end: filters.maxPrice,
      };
    }
  },


  CLEAR_FILTER(state, type) {
    if (type === 'price') {
      state.filters.selected[type] = {
        start: 0,
        end: 0,
      };
      return;
    }

    state.filters.selected[type] = [];
  },

  CLEAR_FILTERS(state) {
    state.filters.selected = {
      ...initStateFilters,
    };
  },

  SET_PAGE(state, value) {
    state.page = value;
  },

  SET_BACK_TO_CATALOG(state, { detailUrl = '', scrollPoint = 0 }) {
    state.backToCatalog = {
      detailUrl,
      scrollPoint,
    };
  },

  FETCHING(state, field) {
    if (!field) { return; }
    state[field].isFetching = true;
  },

  FETCHED_BANNER(state, data) {
    state.banners.isFetching = false;
    state.banners.data = data;
  },
};

export const actions = {

  FETCH_PRODUCTS({ commit, state }, params) {
    if (state.products.isFetching) {
      state.products.cancelToken.cancel();
    }

    const feedCancelSource = CancelToken.source();
    commit('FETCHING_PRODUCTS', feedCancelSource);

    const sortAdapted = sortAdapter(state);

    return this.$axios.$get('frontend/v1/catalog/product/list/', {
      // useCache: true,
      params: {
        ...params,
        ...sortAdapted,
      },
      cancelToken: feedCancelSource.token,
    })
      .then(({ DATA }) => {
        commit('FETCHED_PRODUCTS', {
          products: DATA.items,
          length: DATA.itemsQuantity,
          page: params.page,
        });
        commit('CLEAR_FILTERS');
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_FILTERS({ commit }, data) {
    return this.$axios.$post('frontend/v1/catalog/get-filters/', data)
      .then(({ DATA }) => {
        commit('FETCHED_FILTERS', DATA.filters);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_FILTERED_PRODUCTS({ commit, state, dispatch }, {
    page, limit, query, ...params
  }) {
    if (state.products.isFetching) {
      state.products.cancelToken.cancel();
    }

    const feedCancelSource = CancelToken.source();
    commit('FETCHING_PRODUCTS', feedCancelSource);
    commit('SET_SELECTED_FILTERS', query);

    const sortAdapted = sortAdapter(state);
    const filtersSelected = state.filters.selected;

    // TODO: блокирую PRICE
    const filtersNotNULL = Object.keys(filtersSelected)
      .filter((key) => {
        if (key === 'price') {
          return filtersSelected.price.start || filtersSelected.price.end;
        }
        const filterSelected = filtersSelected[key];
        return Array.isArray(filterSelected) ? filterSelected.length : true;
      });

    if (!filtersNotNULL.length) {
      return dispatch('FETCH_PRODUCTS', {
        ...params,
        page,
        limit: 40,
      });
    }

    const { price, ...paramsFilters } = filtersSelected;

    const paramsFilterPrice = filtersNotNULL
      .filter(key => key === 'price')
      .reduce((acc, key) => ({
        ...acc,
        minPrice: filtersSelected[key].start,
        maxPrice: filtersSelected[key].end,
      }), {});

    const data = {
      ...params,
      page,
      limit,
      ...paramsFilters,
      ...paramsFilterPrice,
      ...sortAdapted,
    };

    return this.$axios({
      method: 'post',
      url: 'frontend/v1/catalog/filters/',
      data,
      cancelToken: feedCancelSource.token,
    })
      .then(({ data: { DATA } }) => {
        commit('FETCHED_PRODUCTS', {
          products: DATA.items,
          length: DATA.itemsQuantity,
          page,
        });
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_CLEAR_FILTER({ commit, dispatch }, { type, params }) {
    commit('CLEAR_FILTER', type);
    return dispatch('FETCH_FILTERED_PRODUCTS', params);
  },

  SELECT_CATEGORY({ commit }, category) {
    commit('SET_SELECTED_CATEGORY', category);
  },
  SELECT_SUBCATEGORY({ commit }, subCategory) {
    commit('SET_SELECTED_SUBCATEGORY', subCategory);
  },

  FETCH_BANNER({ commit }, data) {
    commit('FETCHING', 'banners');
    return this.$axios.$post('frontend/v1/banners/catalog-left/', data)
      .then(({ DATA }) => {
        commit('FETCHED_BANNER', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
