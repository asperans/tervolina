/* eslint-disable no-shadow */
// Корневой модуль. Используется для служебных данных, не связанных с контентом,
// а также для запуска nuxtServerInit
// https://nuxtjs.org/guide/vuex-store#the-nuxtserverinit-action
import getCookie from '@/helpers/getCookie';

export const state = () => ({
  errors: [],
  cookiePermission: true,
  sessionId: '',
});

export const mutations = {
  ADD_ERROR(state, payload) {
    state.errors = [
      ...state.errors,
      {
        timestamp: new Date().getTime(),
        message: payload,
        isConfirmed: false,
      },
    ];
  },

  CONFIRM_ERROR(state, timestamp) {
    state.errors = state.errors.map((item) => {
      const newItem = item;
      if (newItem.timestamp === timestamp) {
        newItem.isConfirmed = true;
      }
      return newItem;
    });
  },

  CLEAR_ERRORS(state) {
    state.errors = [];
  },

  SET_COOKIE_PERMISSON(state, payload) {
    state.cookiePermission = payload;
    document.cookie = `cookiePermission=${payload}`;
  },

  SET_SESSION_ID(state, payload) {
    state.sessionId = payload;
  },
};

export const actions = {
  nuxtServerInit({ dispatch, rootState }, { route, req }) {
    const url = route.path;
    const requests = [
      dispatch('layout/FETCH_DEPARTMENTS', url),
      dispatch('user/FETCH_PERSONAL'),
      dispatch('basket/FETCH_PRODUCTS'),
    ];
    if (!getCookie('confirmedCity', req.headers.cookie)) {
      requests.push(dispatch('user/FETCH_CITY'));
    }
    return Promise.all(requests)
      .then(() => dispatch('layout/FETCH_TOP_MENU', rootState.layout.currentDepartment.id))
      .catch(() => {});
  },
};
