/* eslint-disable no-shadow */
// Модуль для работы с бонусами
export const state = () => ({
  themes: {
    isFetching: false,
    data: [],
  },
  add: {
    isFetching: false,
  },
});

export const mutations = {
  FETCHING(state, field) {
    if (!field) { return; }
    state[field.toLowerCase()].isFetching = true;
  },

  FETCHED(state, field) {
    if (!field) { return; }
    state[field.toLowerCase()].isFetching = false;
  },

  FETCHED_THEMES(state, data) {
    state.themes.isFetching = false;
    state.themes.data = data;
  },
};


export const actions = {
  FETCH_THEMES({ commit }) {
    commit('FETCHING', 'THEMES');
    return this.$axios.$get('frontend/v1/feedback/themes/')
      .then(({ DATA }) => {
        commit('FETCHED_THEMES', DATA);
      })
      .catch(error => Promise.reject(error));
  },


  FETCH_ADD_FEEDBACK({ commit }, data) {
    commit('FETCHING', 'ADD');
    return this.$axios.$post('frontend/v1/feedback/add/', data)
      .then(({ STATUS }) => {
        commit('FETCHED', 'ADD');
        return STATUS;
      })
      .catch(error => Promise.reject(error));
  },
};
