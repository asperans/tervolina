/* eslint-disable no-shadow */
// Модуль данных для блоков главной страницы
export const state = () => ({
  promo: {
    slider: {
      woman: {
        isFetching: false,
        data: [],
      },
      man: {
        isFetching: false,
        data: [],
      },
      children: {
        isFetching: false,
        data: [],
      },
    },
    banners: {
      woman: {
        isFetching: false,
        data: [],
      },
      man: {
        isFetching: false,
        data: [],
      },
      children: {
        isFetching: false,
        data: [],
      },
    },
  },
  promocode: {
    woman: {
      isFetching: false,
      data: [],
    },
    man: {
      isFetching: false,
      data: [],
    },
    children: {
      isFetching: false,
      data: [],
    },
  },
  lastArticles: {
    woman: {
      isFetching: false,
      data: [],
    },
    man: {
      isFetching: false,
      data: [],
    },
    children: {
      isFetching: false,
      data: [],
    },
  },
  popularArticles: {
    isFetching: false,
    data: [],
  },
  instagram: {
    isFetching: false,
    data: [],
  },
  newProducts: {
    isFetching: false,
    data: {},
  },
});

export const mutations = {
  FETCHING_PROMO_WOMAN_SLIDER(state) {
    state.promo.slider.woman.isFetching = true;
  },

  FETCHED_PROMO_WOMAN_SLIDER(state, data) {
    state.promo.slider.woman.isFetching = false;
    state.promo.slider.woman.data = data;
  },

  FETCHING_PROMO_MAN_SLIDER(state) {
    state.promo.slider.man.isFetching = true;
  },

  FETCHED_PROMO_MAN_SLIDER(state, data) {
    state.promo.slider.man.isFetching = false;
    state.promo.slider.man.data = data;
  },

  FETCHING_PROMO_CHILDREN_SLIDER(state) {
    state.promo.slider.children.isFetching = true;
  },

  FETCHED_PROMO_CHILDREN_SLIDER(state, data) {
    state.promo.slider.children.isFetching = false;
    state.promo.slider.children.data = data;
  },

  FETCHING_PROMO_WOMAN_BANNERS(state) {
    state.promo.banners.woman.isFetching = true;
  },

  FETCHED_PROMO_WOMAN_BANNERS(state, data) {
    state.promo.banners.woman.isFetching = false;
    state.promo.banners.woman.data = data;
  },

  FETCHING_PROMO_MAN_BANNERS(state) {
    state.promo.banners.man.isFetching = true;
  },

  FETCHED_PROMO_MAN_BANNERS(state, data) {
    state.promo.banners.man.isFetching = false;
    state.promo.banners.man.data = data;
  },

  FETCHING_PROMO_CHILDREN_BANNERS(state) {
    state.promo.banners.children.isFetching = true;
  },

  FETCHED_PROMO_CHILDREN_BANNERS(state, data) {
    state.promo.banners.children.isFetching = false;
    state.promo.banners.children.data = data;
  },

  FETCHING_LAST_ARTICLES_WOMAN(state) {
    state.lastArticles.woman.isFetching = true;
  },

  FETCHED_LAST_ARTICLES_WOMAN(state, data) {
    state.lastArticles.woman.isFetching = false;
    state.lastArticles.woman.data = data;
  },

  FETCHING_LAST_ARTICLES_MAN(state) {
    state.lastArticles.man.isFetching = true;
  },

  FETCHED_LAST_ARTICLES_MAN(state, data) {
    state.lastArticles.man.isFetching = false;
    state.lastArticles.man.data = data;
  },

  FETCHING_LAST_ARTICLES_CHILDREN(state) {
    state.lastArticles.children.isFetching = true;
  },

  FETCHED_LAST_ARTICLES_CHILDREN(state, data) {
    state.lastArticles.children.isFetching = false;
    state.lastArticles.children.data = data;
  },

  FETCHING_POPULAR_ARTICLES(state) {
    state.popularArticles.isFetching = true;
  },

  FETCHED_POPULAR_ARTICLES(state, data) {
    state.popularArticles.isFetching = false;
    state.popularArticles.data = data;
  },

  FETCHING_INSTAGRAM(state) {
    state.instagram.isFetching = true;
  },

  FETCHED_INSTAGRAM(state, data) {
    state.instagram.isFetching = false;
    state.instagram.data = data;
  },

  FETCHING_NEW_PRODUCTS(state) {
    state.newProducts.isFetching = true;
  },

  FETCHED_NEW_PRODUCTS(state, data) {
    state.newProducts.isFetching = false;
    state.newProducts.data = data;
  },

  FETCHING_PROMOCODE_WOMAN(state) {
    state.promocode.woman.isFetching = true;
  },

  FETCHED_PROMOCODE_WOMAN(state, data) {
    state.promocode.woman.isFetching = false;
    state.promocode.woman.data = data;
  },

  FETCHING_PROMOCODE_MAN(state) {
    state.promocode.woman.isFetching = true;
  },

  FETCHED_PROMOCODE_MAN(state, data) {
    state.promocode.man.isFetching = false;
    state.promocode.man.data = data;
  },

  FETCHING_PROMOCODE_CHILDREN(state) {
    state.promocode.children.isFetching = true;
  },

  FETCHED_PROMOCODE_CHILDREN(state, data) {
    state.promocode.children.isFetching = false;
    state.promocode.children.data = data;
  },
};

export const actions = {
  FETCH_PROMO_WOMAN_SLIDER({ commit }) {
    commit('FETCHING_PROMO_WOMAN_SLIDER');
    return this.$axios.$get('frontend/v1/slider/woman/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMO_WOMAN_SLIDER', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMO_MAN_SLIDER({ commit }) {
    commit('FETCHING_PROMO_MAN_SLIDER');
    return this.$axios.$get('frontend/v1/slider/man/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMO_MAN_SLIDER', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMO_CHILDREN_SLIDER({ commit }) {
    commit('FETCHING_PROMO_CHILDREN_SLIDER');
    return this.$axios.$get('frontend/v1/slider/children/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMO_CHILDREN_SLIDER', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMO_WOMAN_BANNERS({ commit }) {
    commit('FETCHING_PROMO_WOMAN_BANNERS');
    return this.$axios.$get('frontend/v1/banner/woman/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMO_WOMAN_BANNERS', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMO_MAN_BANNERS({ commit }) {
    commit('FETCHING_PROMO_MAN_BANNERS');
    return this.$axios.$get('frontend/v1/banner/man/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMO_MAN_BANNERS', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMO_CHILDREN_BANNERS({ commit }) {
    commit('FETCHING_PROMO_CHILDREN_BANNERS');
    return this.$axios.$get('frontend/v1/banner/children/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMO_CHILDREN_BANNERS', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMOCODE_WOMAN({ commit }) {
    commit('FETCHING_PROMOCODE_WOMAN');
    return this.$axios.$get('frontend/v1/index/promocode/woman/', {
      // useCache: true,
    })
      .then(({ DATA }) => {
        commit('FETCHED_PROMOCODE_WOMAN', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMOCODE_MAN({ commit }) {
    commit('FETCHING_PROMOCODE_MAN');
    return this.$axios.$get('frontend/v1/index/promocode/man/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMOCODE_MAN', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PROMOCODE_CHILDREN({ commit }) {
    commit('FETCHING_PROMOCODE_CHILDREN');
    return this.$axios.$get('frontend/v1/index/promocode/children/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_PROMOCODE_CHILDREN', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_LAST_ARTICLES_WOMAN({ commit }) {
    commit('FETCHING_LAST_ARTICLES_WOMAN');
    return this.$axios.$get('frontend/v1/blog/list/woman/', {
      params: {
        limit: 3,
        page: 1,
      },
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_LAST_ARTICLES_WOMAN', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_LAST_ARTICLES_MAN({ commit }) {
    commit('FETCHING_LAST_ARTICLES_MAN');
    return this.$axios.$get('frontend/v1/blog/list/man/', {
      params: {
        limit: 3,
        page: 1,
      },
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_LAST_ARTICLES_MAN', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_LAST_ARTICLES_CHILDREN({ commit }) {
    commit('FETCHING_LAST_ARTICLES_CHILDREN');
    return this.$axios.$get('frontend/v1/blog/list/children/', {
      params: {
        limit: 3,
        page: 1,
      },
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_LAST_ARTICLES_CHILDREN', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_POPULAR_ARTICLES({ commit }) {
    commit('FETCHING_POPULAR_ARTICLES');
    return this.$axios.$get('frontend/v1/blog/popular-list/', {
      params: {
        limit: 4,
        page: 1,
      },
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_POPULAR_ARTICLES', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_INSTAGRAM({ commit }) {
    commit('FETCHING_INSTAGRAM');
    return this.$axios.$get('frontend/v1/instagram/list/', {
      // useCache: true,
    })
      .then((response) => {
        commit('FETCHED_INSTAGRAM', response.DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_NEW_PRODUCTS({ commit }, { id }) {
    commit('FETCHING_NEW_PRODUCTS');
    return this.$axios.$get('frontend/v1/catalog/product/new-list/',
      {
        params: {
          'section-id': id,
          page: 1,
          limit: 8,
          // useCache: true,
        },
      })
      .then(({ DATA }) => {
        commit('FETCHED_NEW_PRODUCTS', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  SUBSCRIBE_EMAIL(context, data) {
    return this.$axios.$post('frontend/v1/forms/subscribe/', data)
      .then(response => response.MESSAGE)
      .catch(error => Promise.reject(error));
  },
};
