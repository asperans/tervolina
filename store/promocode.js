/* eslint-disable no-shadow */
// Модуль для работы с промокодом
export const state = () => ({
  code: '',
  isActive: false,
  isSubmitted: false,
  isFetching: false,
  discountAmount: 0,
  discount: 0,
});

export const mutations = {
  SET_CODE(state, payload) {
    state.code = payload;
  },

  SET_SUBMITTED(state, payload) {
    state.isSubmitted = payload;
  },

  SET_ACTIVE(state, payload) {
    state.isActive = payload;
  },

  SET_SELECTED(state, payload) {
    state.discountAmount = payload;
  },

  FETCHING_PROMOCODE(state) {
    state.isFetching = true;
  },

  FETCHED_PROMOCODE(state, { label, discount, discountAmount }) {
    state.isFetching = false;
    state.discountAmount = discountAmount;
    state.discount = discount;
    state.code = label || state.code;
    state.isSubmitted = true;
    state.isActive = true;
  },

  FETCHED_CHECK_PROMOCODE(state) {
    state.isFetching = false;
  },

  CLEAR_PROMOCODE(state) {
    state.code = '';
    state.isActive = false;
    state.isFetching = false;
    state.discountAmount = 0;
    state.discount = 0;
    state.isSubmitted = false;
  },
};

export const actions = {
  FETCH_PROMOCODE({ commit }, data) {
    commit('FETCHING_PROMOCODE');
    return this.$axios.$post('frontend/v1/cart/promocode/', data)
      .then(({ DATA }) => {
        if (DATA.promocode) {
          commit('FETCHED_PROMOCODE', DATA.promocode);
        }
        commit('basket/FETCHED_UPDATE_PRODUCT', DATA, { root: true });
        commit('SET_SUBMITTED', true);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_CHECK_PROMOCODE({ commit }, data) {
    commit('FETCHING_PROMOCODE');
    return this.$axios.$post('frontend/v1/cart/promocode/check/', data)
      .then((response) => {
        commit('FETCHED_CHECK_PROMOCODE');
        return response;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_REMOVE_PROMOCODE({ commit }) {
    commit('FETCHING_PROMOCODE');
    return this.$axios.$get('frontend/v1/cart/promocode/delete/')
      .then(({ DATA }) => {
        if (DATA.promocode) {
          commit('FETCHED_PROMOCODE', DATA.promocode);
        }
        commit('basket/FETCHED_UPDATE_PRODUCT', DATA, { root: true });
        commit('CLEAR_PROMOCODE');
      })
      .catch(error => Promise.reject(error));
  },
};
