/* eslint-disable no-shadow */
// Модуль для работы с данными общими для всех страниц стандартного шаблона и
// шаблона корзины
export const state = () => ({
  departments: {
    isFetching: false,
    data: [],
  },
  currentDepartment: {},
  currentCategory: {},
  isMobileMenuOpen: false,
  isMobile: false,
  paddingTop: 0,
  topMenu: {
    isFetching: false,
    data: {},
  },
});

export const mutations = {
  FETCHING_DEPARTMENTS(state) {
    state.departments.isFetching = true;
  },

  FETCHED_DEPARTMENTS(state, data) {
    state.departments.isFetching = false;
    state.departments.data = data;
  },

  SET_CURRENT_DEPARTMENT(state, data) {
    state.currentDepartment = data;
  },

  SET_CURRENT_CATEGORY(state, data) {
    state.currentCategory = data;
  },

  SET_MOBILE_MENU(state, data) {
    state.isMobileMenuOpen = data;
  },

  SET_MOBILE(state, data) {
    state.isMobile = data;
  },

  SET_PADDING_TOP(state, data) {
    state.paddingTop = data;
  },

  FETCHING_TOP_MENU(state) {
    state.topMenu.isFetching = true;
  },

  FETCHED_TOP_MENU(state, data) {
    state.topMenu.isFetching = false;
    state.topMenu.data = data;
  },

};

export const actions = {
  FETCH_DEPARTMENTS({ commit }, url) {
    commit('FETCHING_DEPARTMENTS');
    return this.$axios.$get('frontend/v1/catalog/section/top-list/')
      .then(({ DATA }) => {
        commit('FETCHED_DEPARTMENTS', DATA);
        const currentDepartment = DATA
          .find(({ url: urlDepartment }) => url === urlDepartment) || DATA[0];

        if (!currentDepartment.id) { return; }

        commit('SET_CURRENT_DEPARTMENT', currentDepartment);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_TOP_MENU({ commit }, departmentId) {
    commit('FETCHING_TOP_MENU');
    return this.$axios.$post('frontend/v1/catalog/banners/top-menu/', {
      departmentId,
    })
      .then(({ DATA }) => {
        commit('FETCHED_TOP_MENU', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
