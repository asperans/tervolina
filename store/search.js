/* eslint-disable no-shadow */
export const state = () => ({
  query: '',
  results: {
    isFetching: false,
    data: {
      items: [],
      itemsQuantity: '',
    },
  },
});

export const mutations = {
  SET_QUERY(state, payload) {
    state.query = payload;
  },

  FETCHING_RESULTS(state) {
    state.results.isFetching = true;
  },

  FETCHED_RESULTS(state, payload) {
    state.results.isFetching = false;
    state.results.data = payload;
  },
};

export const actions = {
  async FETCH_RESULTS({ commit }, params) {
    commit('FETCHING_RESULTS');
    const { DATA } = await this.$axios.$get(
      'frontend/v1/catalog/search/',
      { params },
    );
    commit('FETCHED_RESULTS', DATA);
  },
};
