
/* eslint-disable no-shadow */
export const state = () => ({
  currentBarCode: 0,
  params: {
    isFetching: false,
    data: {},
  },
  info: {
    isFetching: false,
    data: {},
  },
  buyTogetherCheaper: {
    isFetching: false,
    data: {},
  },
  activeColor: {
    index: 0,
    id: 0,
    name: '',
  },
  activeSizeId: 0,
});

export const mutations = {
  FETCHING_PARAMS(state) {
    state.params.isFetching = true;
  },

  FETCHED_PARAMS(state, data) {
    state.params.isFetching = false;
    state.params.data = data;
  },

  FETCHING_INFO(state) {
    state.info.isFetching = true;
  },

  FETCHED_INFO(state, data) {
    state.info.isFetching = false;
    state.info.data = data;
  },

  FETCHING_BUY_TOGETHER_CHEAPER(state) {
    state.buyTogetherCheaper.isFetching = true;
  },

  FETCHED_BUY_TOGETHER_CHEAPER(state, data) {
    state.buyTogetherCheaper.isFetching = false;
    state.buyTogetherCheaper.data = data;
  },

  SET_PRICE(state, { price, oldPrice, discount }) {
    state.params.data = {
      ...state.params.data,
      price,
      oldPrice,
      discount,
    };
  },

  SET_FAVOURITE(state, value) {
    state.params.data.isFavourite = value;
  },

  SET_CURRENT_BAR_CODE(state, value) {
    state.currentBarCode = value;
  },

  SET_ACTIVE_COLOR(state, color) {
    state.activeColor = { ...color };
  },

  SET_ACTIVE_SIZE(state, id = 0) {
    state.activeSizeId = id;
  },

  SET_BOOKED_COLOR(state, id) {
    state.params.data.colors = state.params.data.colors
      .map(color => (id === color.id
        ? { ...color, isBooked: true } : color));
  },

  SET_BOOKED_SIZE(state, id) {
    const colors = state.params.data.colors
      .map((color) => {
        const sizes = color.sizes
          .map(size => (id === size.id
            ? { ...size, isBooked: true }
            : size));

        return {
          ...color,
          sizes,
        };
      });

    state.params.data.colors = [
      ...colors,
    ];
  },
};

export const actions = {
  SET_BOOKED({ commit }, { type, id }) {
    const mapCommit = {
      color: 'SET_BOOKED_COLOR',
      size: 'SET_BOOKED_SIZE',
    };

    commit(mapCommit[type], id);
  },

  FETCH_PARAMS({ commit }, id) {
    commit('FETCHING_PARAMS');
    return this.$axios.$get('frontend/v1/catalog/product/detail/params/', {
      // useCache: true,
      params: {
        id,
      },
    })
      .then(({ DATA }) => {
        commit('FETCHED_PARAMS', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_INFO({ commit }, id) {
    commit('FETCHING_INFO');
    return this.$axios.$get('frontend/v1/catalog/product/detail/info/', {
      // useCache: true,
      params: {
        id,
      },
    })
      .then(({ DATA }) => {
        commit('FETCHED_INFO', DATA[0]);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_BUY_TOGETHER_CHEAPER({ commit }, id) {
    commit('FETCHING_BUY_TOGETHER_CHEAPER');
    return this.$axios.$get('frontend/v1/catalog/product/detail/info/', {
      // useCache: true,
      params: {
        id,
      },
    })
      .then(({ DATA }) => {
        commit('FETCHED_BUY_TOGETHER_CHEAPER', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
