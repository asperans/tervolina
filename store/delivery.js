/* eslint-disable no-shadow */
export const state = () => ({
  bitrixId: 0,
  city: {},
  autoSelectCityValue: '',
  address: '',
  addressDadata: {
    data: {},
    value: '',
  },
  index: '',
  desiredDate: '',
  title: '',
  type: '',
  code: '',
  payment: '',
  street: {},
  point: {},
  contacts: {
    name: '',
    lastName: '',
    phone: '',
    email: '',
    shouldSubscribe: '',
  },
  isOrderCompleted: false,
  deliveries: {
    isFetching: false,
    data: [],
  },
  delivery: {
    isFetching: false,
    data: [],
  },
  takeFromStore: {
    isFetching: false,
    data: [],
    price: 0,
    description: '',
  },
  deliverToStore: {
    isFetching: false,
    data: [],
    price: 0,
    description: '',
  },
  courier: {
    isFetching: false,
    data: [],
    price: 0,
    description: '',
  },
  postal: {
    isFetching: false,
    data: [],
    price: 0,
    description: '',
  },
  pvz: {
    price: 0,
    description: '',
  },
  orderInfo: {
    isFetching: false,
    data: [],
  },
  orderResults: {
    isFetching: false,
    data: [],
  },
  payments: {
    isFetching: false,
    data: [],
  },
});

export const mutations = {
  SET_TYPE(state, payload) {
    state.type = payload;
  },

  SET_CITY(state, payload) {
    state.city = payload;
  },

  SET_PRICE(state, payload) {
    state[payload.type].price = payload.price;
  },

  SET_DESCRIPTION(state, payload) {
    state[state.type].description = payload;
  },

  SET_ADDRESS(state, payload) {
    state.address = payload;
  },

  SET_ADDRESS_DADATA(state, payload) {
    state.addressDadata = payload;
  },

  SET_INDEX(state, payload) {
    state.index = payload;
  },

  SET_DESIRED_DATE(state, payload) {
    state.desiredDate = payload;
  },

  SET_TITLE(state, payload) {
    state.title = payload;
  },

  SET_PAYMENT(state, payload) {
    state.payment = payload;
  },

  SET_CONTACTS(state, payload) {
    state.contacts = payload;
  },

  SET_BITRIX_ID(state, payload) {
    state.bitrixId = payload;
  },

  SET_DELIVERY_CODE(state, payload) {
    state.contacts = payload;
  },

  SET_ORDER_COMPLETED(state, payload) {
    state.isOrderCompleted = payload;
  },

  SET_STREET(state, payload) {
    state.street = payload;
  },

  SET_POINT(state, payload) {
    state.point = payload;
  },

  SET_CODE(state, payload) {
    state.code = payload;
  },

  SET_AUTO_SELECT_CITY_VALUE(state, payload) {
    state.autoSelectCityValue = payload;
  },

  CREATING_ORDER(state) {
    state.orderResults.isFetching = true;
  },

  CREATED_ORDER(state, payload) {
    state.orderResults.isFetching = false;
    state.orderResults.data = payload;
  },

  FETCHING_DELIVERIES(state) {
    state.deliveries.isFetching = true;
  },

  FETCHED_DELIVERIES(state, payload) {
    state.deliveries.isFetching = false;
    state.deliveries.data = payload;
  },

  FETCHING_DELIVERY(state) {
    state.delivery.isFetching = true;
  },

  FETCHED_DELIVERY(state, payload) {
    state.delivery.isFetching = false;
    state.delivery.data = payload;
  },

  FETCHING_COURIER(state) {
    state.courier.isFetching = true;
  },

  FETCHED_COURIER(state, payload) {
    state.courier.isFetching = false;
    state.courier.data = payload;
  },

  FETCHING_POSTAL(state) {
    state.postal.isFetching = true;
  },

  FETCHED_POSTAL(state, payload) {
    state.postal.isFetching = false;
    state.postal.data = payload;
  },

  FETCHING_PAYMENTS(state) {
    state.payments.isFetching = true;
  },

  FETCHED_PAYMENTS(state, payload) {
    state.payments.isFetching = false;
    state.payments.data = payload;
  },

  FETCHING_ORDER_INFO(state) {
    state.orderInfo.isFetching = true;
  },

  FETCHED_ORDER_INFO(state, payload) {
    state.orderInfo.isFetching = false;
    state.orderInfo.data = payload;
  },

  CLEAN_ORDER(state) {
    state.bitrixId = 0;
    state.address = '';
    state.addressDadata = {
      data: {},
      value: '',
    };
    state.index = '';
    state.desiredDate = '';
    state.title = '';
    state.type = '';
    state.code = '';
    state.payment = '';
    state.street = {};
    state.point = {};
    state.takeFromStore.price = 0;
    state.deliverToStore.price = 0;
    state.courier.price = 0;
    state.postal.price = 0;
    state.pvz.price = 0;
    state.deliveries.data = [];
    state.delivery.data = [];
    state.courier.data = [];
    state.postal.data = [];
    state.payments.data = [];
  },
};

export const actions = {
  CREATE_ORDER({ commit, state }) {
    commit('CREATING_ORDER');
    return this.$axios.$post('frontend/v1/order/create/', {
      payment: state.payment,
      contacts: state.contacts,
      delivery: {
        id: state.bitrixId,
        description: {
          type: state.type,
          code: state.code,
          region: state.deliveries.data.params.region,
          regionType: state.addressDadata.data.regionType,
          city: state.city.data.city,
          cityType: state.addressDadata.data.cityType,
          street: state.addressDadata.data.street,
          streetType: state.addressDadata.data.streetType,
          house: state.addressDadata.data.house,
          block: state.addressDadata.data.block,
          office: state.addressDadata.data.office,
          address: state.address,
          postalCode: state.city.data.postalCode,
          index: state.index,
          desiredDate: state.desiredDate,
          pvz: state.point.id,
          point: state.point,
          title: state.title,
        },
      },
    })
      .then(({ DATA }) => {
        commit('CREATED_ORDER', DATA);
        commit('SET_ORDER_COMPLETED', true);
        commit('CLEAN_ORDER', true);
        return DATA;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_DELIVERIES({ commit }, data) {
    commit('FETCHING_DELIVERIES');
    commit('SET_TYPE', '');
    return this.$axios.$post('frontend/v1/order/delivery/', data)
      .then(({ DATA }) => {
        commit('FETCHED_DELIVERIES', DATA);
        commit('SET_ORDER_COMPLETED', false);
        DATA.deliveries.forEach((delivery) => {
          const price = delivery.price || delivery.minPrice;
          if (price) {
            commit('SET_PRICE', {
              type: delivery.type,
              price,
            });
          }
        });
        return DATA;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_DELIVERY({ commit }, data) {
    commit('FETCHING_DELIVERY');
    return this.$axios.$post('frontend/v1/order/delivery/info/', data)
      .then(({ DATA }) => {
        commit('FETCHED_DELIVERY', DATA);
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_COURIER({ commit }, data) {
    commit('FETCHING_COURIER');
    return this.$axios.$post('frontend/v1/order/delivery/courier/', data)
      .then(({ DATA }) => {
        commit('FETCHED_COURIER', DATA);

        return DATA;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_POSTAL({ commit }, data) {
    commit('FETCHING_POSTAL');
    return this.$axios.$post('frontend/v1/order/delivery/postal/', data)
      .then(({ DATA }) => {
        commit('FETCHED_POSTAL', DATA);

        return DATA;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_ORDER_INFO({ commit }, data) {
    commit('FETCHING_ORDER_INFO');
    return this.$axios.$post('frontend/v1/order/info/', data)
      .then(({ DATA }) => {
        commit('FETCHED_ORDER_INFO', DATA);

        return DATA;
      })
      .catch(error => Promise.reject(error));
  },

  FETCH_PAYMENTS({ commit }, data) {
    commit('FETCHING_PAYMENTS');
    return this.$axios.$post('frontend/v1/order/payments/', data)
      .then(({ DATA }) => {
        commit('FETCHED_PAYMENTS', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
