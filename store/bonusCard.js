/* eslint-disable no-shadow */
// Модуль для работы с бонусами
export const state = () => ({
  code: '',
  selected: 0,
  available: 700,
  isActive: false,
  binding: 'unbound',
  isFlipped: false,
});

export const mutations = {
  SET_CODE(state, payload) {
    state.code = payload;
  },

  SET_SELECTED(state, payload) {
    state.selected = payload;
  },

  SET_ACTIVE(state, payload) {
    state.isActive = payload;
  },

  SET_BINDING(state, payload) {
    state.binding = payload;
  },

  SET_FLIPPED(state, payload) {
    state.isFlipped = payload;
  },
};
