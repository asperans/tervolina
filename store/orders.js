/* eslint-disable no-shadow */
// Модуль для работы с оформлением заказа
export const state = () => ({
  orders: {
    isFetching: false,
    data: [],
  },
  ordersQuantity: 0,
});

export const mutations = {
  FETCHING_ORDERS(state) {
    state.orders.isFetching = true;
  },

  FETCHED_ORDERS(state, { orders = [], ordersQuantity = 0 }) {
    state.orders.isFetching = false;
    state.orders.data = orders;
    state.ordersQuantity = ordersQuantity;
  },
};

export const actions = {
  FETCH_ORDERS({ commit }, { page = 1, limit = 5 }) {
    commit('FETCHING_ORDERS');
    return this.$axios.$post('frontend/v1/user/order/list/', {
      page,
      limit,
    })
      .then(({ DATA }) => {
        commit('FETCHED_ORDERS', DATA);
      })
      .catch(error => Promise.reject(error));
  },
};
